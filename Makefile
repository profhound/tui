PROGRAM=fptuitreemap
SOURCES=fptuitreemap.pas blocks.pas ezcrt.pas fpsortrecords.pas
OBJS=$(SOURCES:.pas=.o)
all: $(PROGRAM)
#.o: %.pas
fptuitreemap:
	fpc $(PROGRAM)
clean:
	rm -f $(OBJS)
	rm -f $(PROGRAM)
	rm -f *.ppu # http://wiki.freepascal.org/File_extensions#Project_files

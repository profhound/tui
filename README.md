## Text-based user interface (TUI) [![pipeline status](https://gitlab.com/profhound/tui/badges/master/pipeline.svg)](https://gitlab.com/profhound/tui/commits/master)

## Components

- fptuitreemap.pas - Main component atm, attempt to draw a tree map in your terminal. See preview below for example output.
- ezcrt.pas - `ScreenWidth`, `ScreenHeight` and other helper methods.
- ezutils.pas - `StringListFromRes` to read Resources as StringList(s) from exe binary.

## Binaries (Continuous Integration)

- <a href="https://gitlab.com/profhound/tui/-/jobs/223463241/artifacts/raw/deb64/fptuitreemap" download>Download fptuitreemap (deb64)(767KB)</a>
- <a href="https://gitlab.com/profhound/tui/-/jobs/artifacts/master/raw/fptuitreemap?job=compile_macos" download>Download fptuitreemap (macos)(416KB)</a>
- <a href="https://gitlab.com/profhound/tui/-/jobs/artifacts/master/raw/fptuitreemap.exe?job=compile_win64" download>Download fptuitreemap.exe (win64)(502KB)</a>

or

```sh
curl -L https://gitlab.com/profhound/tui/-/jobs/artifacts/master/raw/fptuitreemap?job=compile_macos > fptuitreemap
curl -L https://gitlab.com/profhound/tui/-/jobs/artifacts/master/raw/fptuitreemap?job=compile_deb64 > fptuitreemap
curl -L https://gitlab.com/profhound/tui/-/jobs/artifacts/master/raw/fptuitreemap.exe?job=compile_win64 > fptuitreemap.exe
./fptuitreemap --help # might need to `chmod +x fptuitreemap` to execute binary
```

## Preview

### `fptuitreemap` Help

```sh
-?, --help: This help.
-v, --version: Version.
-f, --file <file>: File to use to read ps listing from. default to ps.tmp if avail, otherwise run the `ps` command below.
                   To generate on a gnu+linux system use: `ps -eo pmem,rss,pid,cmd > ps.tmp`
                   To generate on a macos system use:     `ps -eo pmem,rss,pid,command > ps.tmp`
                   To generate on a busybox system use:   `ps -eo vsz,rss,pid,comm > ps.tmp`
-n, --interval <ms>: Redraw every <ms> milliseconds.
--html <filename>: Creates a Google Chart Treemap HTML file.
--zabbix-discovery [--output <filename>]: Creates a JSON Meta data string with {#NAME} {#PID} object sets.
--zabbix-trapper: Creates a JSON trapper data string.
--readln. Wait after draw for <enter>, --readline=1 by default on windows, except when using a --zabbix-* option.
```

<a href="https://gitlab.com/profhound/tui/-/jobs/artifacts/master/raw/fptuitreemap.exe?job=compile_win64" download>![](fptuitreemap_demo_win.png)</a><br>
<a href="https://gitlab.com/profhound/tui/-/jobs/artifacts/master/raw/fptuitreemap.exe?job=compile_win64" download>Download (win64)(502KB)</a> | <a href="https://gitlab.com/profhound/tui/-/jobs/artifacts/master/raw/fptuitreemap?job=compile_macos" download>Download (macos)(416KB)</a> | <a href="https://gitlab.com/profhound/tui/-/jobs/223463241/artifacts/raw/deb64/fptuitreemap" download>Download (deb64)(767KB)</a>

## Compile from Source(Ubuntu/debian/WSL)

```sh
git clone https://gitlab.com/profhound/tui.git
cd tui
sudo apt-get install fp-compiler fp-units-fcl -y
fpc fptuitreemap.pas
ps -eo pmem,rss,pid,cmd > ps.tmp && ./fptuitreemap
# magic... or ./fptuitreemap --help
```

## Zabbix

For more information on Zabbix see the [What is Zabbix?](https://github.com/zabbix/zabbix-docker#what-is-zabbix) section.

You will need to import `zbx_export_templates.xml` into zabbix and create a `<host_name>` attached to the template for auto discovery and trapping to work.

#### Discovery

```sh
fptuitreemap.exe --zabbix-discovery -h <host_name> -k process.memory.usage -o <output_file.json>
zabbix_sender -z <example.zabbix.server.com> -p <port> -i <output_file.json>
```

#### Trapper

```sh
fptuitreemap.exe --zabbix-trapper -h <host_name> -o <output_file.json>
zabbix_sender -z <example.zabbix.server.com> -p <port> -i <output_file.json>
```

## License

MIT/(L)GPL

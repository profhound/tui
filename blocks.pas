unit blocks;

{$mode objfpc}{$H+}

interface

type
  TBlock = Record
    Area, Nr, Rss : Integer;
    Cmd, PMem, Pid : String;
    PMegaBytes : Double;
    Width, Height, row, col: Byte;
  end;

function BlocksArea: Integer;
function KiloBytesToMegaBytes(aKiloBytes: Integer): Double;

implementation

uses
  ezCrt;

function BlocksArea: Integer;
begin
  Result := ( ScreenWidth ) * ( ScreenHeight );
end;

function KiloBytesToMegaBytes(aKiloBytes: Integer): Double;
begin
  Result := aKiloBytes / 1000; (* why not 1024 ???! *)
end;

end.


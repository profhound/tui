unit ezCrt;

{$mode objfpc}{$H+}

interface

function ScreenWidth: Integer;
function ScreenHeight: Integer;

procedure BlankScreen;
procedure ResetScreen;
procedure RestCursor;

procedure DrawBlock(x1, y1, x2, y2: Integer); // Integer or Byte ???
procedure DrawBlockText(x1, y1, x2, y2: Integer; aText: String);

procedure CheckCtrlC;
procedure SleepCheckCtrlC(aMilSecs: Cardinal);

implementation

uses
  Crt,
  SysUtils; // sleep()

const
  LIB_VERSION = '190418';

var
  OrgWidth, OrgHeight: Integer;

function ScreenWidth: Integer;
begin
  Result := WindMaxX - WindMinX + 1; // https://stackoverflow.com/a/26786100/4434121
end;

function ScreenHeight: Integer;
begin
  Result := WindMaxY - WindMinY + 1; // https://stackoverflow.com/a/26786100/4434121
end;

procedure BlankScreen;
begin
  TextBackground(Black);
  //TextColor(White);
  ResetScreen;
  ClrScr
end;

procedure ResetScreen;
begin
  GotoXY(1, 1);
  Window(1, 1, OrgWidth, OrgHeight); // reset to org terminal size
end;

procedure RestCursor;
begin
  GotoXY( 1, ScreenHeight); // rest curser bottom left
end;

procedure RestrictToScreen(var x1, y1, x2, y2: Integer);
begin
  if x1 > OrgWidth then // do not allow drawing window off screen
    x1 := OrgWidth;
  if y1 > OrgHeight then
   y1 := OrgHeight;
  if x2 > OrgWidth then
    x2 := OrgWidth;
  if y2 > OrgHeight then
   y2 := OrgHeight;
  if x1 < 1 then // also to the left
    x1 := 1;
  if y1 < 1 then
    y1 := 1;
  if x2 < 1 then
    x2 := 1;
  if y2 < 1 then
    y2 := 1;
end;

procedure SaveOrgScreen;
begin
  OrgWidth := ScreenWidth;
  OrgHeight:= ScreenHeight;
end;

procedure DrawBlock(x1, y1, x2, y2: Integer);
begin
  SaveOrgScreen;
  RestrictToScreen(x1, y1, x2, y2);
  Window(x1, y1, x2, y2);
  ClrScr; // Alot faster then a for x for y loop to draw a block
  //ResetScreen;
  //RestCursor;
end;

procedure DrawBlockText(x1, y1, x2, y2: Integer; aText: String);
begin
  DrawBlock(x1, y1, x2, y2);

  //GotoXY(x1,y1);
  Write(aText);

  ResetScreen;
  RestCursor;
end;

procedure CheckCtrlC;
begin
  if KeyPressed and (ReadKey = ^C) then
    Halt;
end;

procedure SleepCheckCtrlC(aMilSecs: Cardinal);
var
  at, steps,
  stepSleep: Cardinal;
begin
  stepSleep := 500;
  steps := aMilSecs div stepSleep; // 500 ms steps to check for ctrl+c
  at := 1;
  while at < steps do
  begin
    sleep(stepSleep);
    CheckCtrlC;
    at := at + 1;
  end;
end;
end.


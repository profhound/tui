unit ezutils;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils; // Exception

function OsProcesses(rssFrom: integer = 0): TStringList;
function GetSystemMemKb: longint;

function StringListFromRes(resName: string): TStringList;

implementation

{$IFDEF WINDOWS}
uses
  jwapsapi,
  LResources,
  Windows,
  jwaTlHelp32,
  JwaWinBase;

function GetSystemMemKb: longint;  // https://stackoverflow.com/a/22987749/4434121
var
  MS_Ex: MemoryStatusEx;
begin
  FillChar(MS_Ex, SizeOf(MemoryStatusEx), #0);
  MS_Ex.dwLength := SizeOf(MemoryStatusEx);
  GlobalMemoryStatusEx(MS_Ex);
  Result := MS_Ex.ullTotalPhys div 1024; // kb
end;


// https://forum.lazarus.freepascal.org/index.php?topic=37493.0
function QueryFullProcessImageNameW(hProcess: THandle; dwFlags: DWORD;
  lpExeName: LPWSTR; var lpdwSize: DWORD): BOOL; stdcall; external kernel32;

function GetProcessPath(PID: DWORD): string;
const
  PROCESS_QUERY_LIMITED_INFORMATION = $1000;
var
  HProcess: THandle;
  WBuf: UnicodeString;
  Len: DWORD;
begin
  Result := '';
  HProcess := OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION, False, PID);
  if HProcess <> 0 then
    try
      SetLength(WBuf, 1000);
      Len := 1000 + 1;
      if QueryFullProcessImageNameW(HProcess, 0, Pointer(WBuf), Len) then
      begin
        SetLength(WBuf, Len);
        Result := UTF8Encode(WBuf);
      end;
    finally
      CloseHandle(HProcess);
    end;
end;

function GetProcessMaxKiloByteFromSetSize(PID: DWORD): SIZE_T;
const
  PROCESS_QUERY_LIMITED_INFORMATION = $1000;
var
  HProcess: THandle;
  minsize, maxsize: SIZE_T;
begin
  Result := -1;
  HProcess := OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION, False, PID);
  if HProcess <> 0 then
    try
      if GetProcessWorkingSetSize(HProcess, minsize, maxsize) then
      begin
        Result := maxsize div 1024;
      end
      else
        raise Exception.Create('Could not GetProcessMaxKiloByteFromSetSize(' + IntToStr(PID) + ')');
    finally
      CloseHandle(HProcess);
    end;
end;


function GetProcessMaxKiloByteFromMemoryInfo(PID: DWORD): SIZE_T;
const
  PROCESS_QUERY_LIMITED_INFORMATION = $1000;
var
  HProcess: THandle;var
  pmc: _PROCESS_MEMORY_COUNTERS;
begin
  Result := -1;
  HProcess := OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION, False, PID);
  if HProcess <> 0 then
    try
      if GetProcessMemoryInfo(HProcess, pmc, SizeOf(_PROCESS_MEMORY_COUNTERS)) then
      begin
        Result := pmc.WorkingSetSize div 1024;
      end
      else
        raise Exception.Create('Could not GetProcessMaxKiloByteFromMemoryInfo(' + IntToStr(PID) + ')');
    finally
      CloseHandle(HProcess);
    end;
end;

function GetProcessMaxKiloByte(PID: DWORD): SIZE_T;
begin
  Result := GetProcessMaxKiloByteFromSetSize(PID);
end;

function OsProcesses(rssFrom: integer = 0): TStringList;
var
  Snapshot: THandle;
  ProcInfo: TProcessEntry32;
  slResult: TStringList;
  strLine, cmd, ppath: string;
  processID, pmem, rss: integer;

begin
  try
    slResult := TStringList.Create;
    slResult.add('ps -eo pmem,rss,pid,cmd > ps.tmp;');
    // add a fake heading for fptuitreemap to ignore
    Snapshot := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
    // https://stackoverflow.com/a/2617364
    if Snapshot <> INVALID_HANDLE_VALUE then
      try
        ProcInfo.dwSize := SizeOf(ProcInfo);
        if Process32First(Snapshot, ProcInfo) then
          repeat
            processID := ProcInfo.th32ProcessID;
            //if processID = 0 then
            //  Continue;
            try
              if rssFrom = 0 then
                rss := GetProcessMaxKiloByteFromMemoryInfo(processID)
              else if rssFrom = 1 then
                rss := GetProcessMaxKiloByteFromSetSize(ProcInfo.th32ProcessID);
            except
              on E : Exception do begin
                writeln(E.Message);
                Continue;
              end;
            end;
            //ppath := GetProcessPath(ProcInfo.th32ProcessID);
            ppath := '';
            pmem := -1; // need to calc % TODO
            cmd := ProcInfo.szExeFile;
            strLine := IntToStr(pmem) + ' ' + IntToStr(rss) + ' ' +
              IntToStr(processID) + ' ' + cmd + ' ' + ppath;
            slResult.add(strLine);
          until not Process32Next(Snapshot, ProcInfo);
      finally
        CloseHandle(Snapshot);
      end;
  finally
    // slResult.free;
    Result := slResult;
  end;
end;

{$ENDIF}
{$IFDEF UNIX}

function OsProcesses(rssFrom: integer = 0): TStringList;
begin
  raise Exception.Create('OsProcesses: should only be called from windows.');
end;

function GetSystemMemKb: longint;
begin
  raise Exception.Create('GetSystemMemKb: should only be called from windows.');
end;

{$ENDIF}


function StringListFromRes(resName: string): TStringList;
var
  RS: TResourceStream;
  SL: TStringList;
begin
  RS := TResourceStream.Create(HInstance, resName, RT_RCDATA);
  try
    SL := TStringList.Create;
    SL.LoadFromStream(RS);
    Result := SL; // SL.FList[0].FString
  finally
    RS.Free;
  end;
end;




end.

unit fpsortrecords;
// idea sourced from https://forum.lazarus.freepascal.org/index.php?topic=40574.0
// might drop in the future and force you to `ps -eo pmem,rss,pid,cmd | sort | fptuitreemap`
// but for now we read it out of ps.txt

{$mode objfpc}{$H+}

interface

uses blocks; // instead of uses my recod here make SortRecords take a generic record TODO!

procedure SortRecords(var aArray :Array of TBlock; aCount: Integer); // Replace with TList.Sort?

implementation

procedure SwapRecords(var aArray :Array of TBlock; idx1, idx2:integer);inline;
var
  vTmp: TBlock;
begin
  vTmp      :=  aArray[idx1];
  aArray[idx1] := aArray[idx2];
  aArray[idx2] := vTmp;
end;

procedure SortRecords(var aArray :Array of TBlock; aCount: Integer); // Replace with TList.Sort?
var
  p, q   : Integer;
  index  : Integer;
begin
  for p := 0 to aCount do
  begin
    index := aArray[p].Area;
    q := p;
    while ((q > 0) AND (aArray[q-1].Area > index)) do
    begin
      SwapRecords(aArray, q, q-1);
      q := q - 1;
    end;
    aArray[q].Area:= index;
  end;
end;

end.


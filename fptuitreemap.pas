program fptuitreemap;

{$mode OBJFPC}{$H+}

uses
  { os }
  {$IFDEF WINDOWS}
  {$r html.rc } { --html windows only atm }
  Windows, { for setconsoleoutputcp }
  {$ENDIF}
  {$IFDEF UNIX}
  {$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}
  Unix,
  {$ENDIF}
  { fcl }
  Classes,
  SysUtils,
  StrUtils,
  CustApp,
  Process,
  Crt,
  { tui }
  ezCrt,
  fpSortRecords,
  Blocks,
  ezUtils;

const
  APP_VERSION = '190206.NOGITREV';

type
  TUIModes = (tuiDebug, tuiReadline, tuiWindows, tuiHtml, tuiZabbix, tuiZabbixDiscovery, tuiZabbixTrapper);

  { TCLIApplication }

  TCLIApplication = class(TCustomApplication)
  private
    appMode: set of TUIModes;
    Processes: Array of TBlock;
    ProcessLines: TStringList;
    SavedScreenWidth: Integer;
    { Totals }
    AtCount, TotalKiloBytes, KiloBytesInABlock, ProcessesCount, TotalSystemMemKiloBytes: Integer;
    { Some helper vars }
    x1, y1, x2, y2, Width, Height, color, atCol, atRow, PrevHeight, AtRowWidth, atColHeight: Byte;
    psFilename : AnsiString; // part of class to show if err
    SplitLine: TStringList;

    procedure WriteHelp;
    procedure CheckOptions;
    procedure Main;
    procedure PopulateProcesses;
    procedure GetProcessLinesFromOS;
    procedure ScreenDraw;
    procedure GoogleChartTreeMap;
    procedure ZabbixDiscovery;
    procedure ZabbixTrapper;
    {$IFDEF WINDOWS}
    procedure GetProcessLinesFromOSWindows;
    {$ENDIF}
    {$IFDEF UNIX}
    procedure GetProcessLinesFromOSUnix;
    {$ENDIF}
  protected
    procedure DoRun; override;
  public
  end;

procedure TCLIApplication.WriteHelp;
begin
  WriteLn(StdOut, ParamStr(0) + ' version ' + APP_VERSION);
  WriteLn(StdOut);
  WriteLn(StdOut, '-?, --help: This help.');
  WriteLn(StdOut, '-v, --version: Version.');
  WriteLn(StdOut, '-f, --file <file>: File to use to read ps listing from. default to ps.tmp if avail, otherwise run the `ps` command below.');
  WriteLn(StdOut, '                   To generate on a gnu+linux system use: `ps -eo pmem,rss,pid,cmd > ps.tmp`');
  WriteLn(StdOut, '                   To generate on a macos system use:     `ps -eo pmem,rss,pid,command > ps.tmp`');
  WriteLn(StdOut, '                   To generate on a busybox system use:   `ps -eo vsz,rss,pid,comm > ps.tmp`');
  WriteLn(StdOut, '-n, --interval <ms>: Redraw every <ms> milliseconds.');
  WriteLn(StdOut, '--html <filename>: Creates a Google Chart Treemap HTML file.');
  WriteLn(StdOut, '--zabbix-discovery [--output <filename>]: Creates a JSON Meta data string with {#NAME} {#PID} object sets.');
  WriteLn(StdOut, '--zabbix-trapper: Creates a JSON trapper data string.');
  WriteLn(StdOut, '--readln. Wait after draw for <enter>, --readline=1 by default on windows, except when using a --zabbix-* option.');
  WriteLn(StdOut, '--reset. Clear screen and reset terminal display.');
  WriteLn(StdOut, '...');
end;

procedure TCLIApplication.CheckOptions;
begin
  if HasOption('?', 'help') then
  begin
    WriteHelp;
    Halt;
  end;
  if HasOption('v', 'version') then
  begin
    writeln(StdOut, APP_VERSION);
    Halt;
  end;
  if HasOption('reset') then
  begin
    TextColor(LightGray);
    BlankScreen;
    Halt;
  end;
  if HasOption('debug') then
    appMode := appMode + [tuiDebug];
  if Self.HasOption('readln') then
    appMode := appMode + [tuiReadline];
  if HasOption('html') then
    appMode := appMode + [tuiHtml];
  if HasOption('zabbix-discovery') then
    appMode := appMode + [tuiZabbix, tuiZabbixDiscovery];
  if HasOption('zabbix-trapper') then
    appMode := appMode + [tuiZabbix, tuiZabbixTrapper];
end;

procedure TCLIApplication.Main;
begin
  PopulateProcesses;
  if tuiHtml in appMode then
  begin
    GoogleChartTreeMap;
  end else if tuiZabbixDiscovery in appMode then
  begin
    ZabbixDiscovery;
  end else if tuiZabbixTrapper in appMode then
  begin
    ZabbixTrapper;
  end else
    ScreenDraw;
end;

procedure TCLIApplication.PopulateProcesses;
var
  sRss: AnsiString;
  psLoadFromFile : Boolean = False;
  At: Integer;
begin
  if HasOption('f', 'file') then
    psFilename := GetOptionValue('f', 'file')
  else
    psFilename := 'ps.tmp'; // ps -eo pmem,rss,pid,cmd > ps.tmp
  psLoadFromFile := FileExists(psFilename);
  try
    if psLoadFromFile then
    begin
      ProcessLines := TStringList.Create;
      ProcessLines.LoadFromFile(psFilename);
    end else
      GetProcessLinesFromOS;

    TotalKiloBytes := 0;
    SetLength(Processes, ProcessLines.Count - 1); // -1 to for heading
    ProcessesCount := 0;
    for at:= 1 to ProcessLines.Count - 1 do
    begin
      try
        SplitLine := TStringList.Create;
        // SplitLine.StrictDelimiter:=; // no strict spaces
        SplitLine.Delimiter:= ' ';
        SplitLine.DelimitedText:= ProcessLines[at];

        Processes[at - 1].PMem:= SplitLine[0];
        sRss := SplitLine[1];
        if pos('m', sRss) > 0 then // busy box report some rss in megabytes
        begin
          sRss := ReplaceStr(sRss,'m','000');
        end;
        Processes[at - 1].Rss := StrToInt(sRss);
        Processes[at - 1].Area := Processes[at - 1].Rss;
        Processes[at - 1].PMegaBytes:= KiloBytesToMegaBytes(StrToInt(SplitLine[1]));
        Processes[at - 1].Pid:= SplitLine[2];
        Processes[at - 1].Cmd:= SplitLine[3];

        TotalKiloBytes := TotalKiloBytes + Processes[at - 1].Area;
      finally
        SplitLine.Free;
      end;
      ProcessesCount := ProcessesCount + 1;
    end;
    KiloBytesInABlock := Round(TotalKiloBytes / BlocksArea);
    // double pass :( after calc Total
    for at:= 0 to ProcessesCount - 1 do
    begin
      if Processes[at].PMem = '-1' then
      begin
        Processes[at].PMem := FloatToStrF( Processes[at].Rss / TotalSystemMemKiloBytes * 100,ffFixed,8,2); // FloatToStr( Processes[at].Rss / TotalKiloBytes);
      end;
      Processes[at].Area :=  Round(Processes[at].Area / KiloBytesInABlock); // fix our area
    end;
  finally
    ProcessLines.Free;
  end;
end;

procedure TCLIApplication.GetProcessLinesFromOS;
begin
  {$IFDEF WINDOWS}
  GetProcessLinesFromOSWindows;
  {$ENDIF}
  {$IFDEF UNIX}
  GetProcessLinesFromOSUnix;
  {$ENDIF}
end;

procedure TCLIApplication.ScreenDraw;
var
  At: Integer;
  Text: AnsiString;
begin
  SavedScreenWidth := ScreenWidth;
  BlankScreen;

  if tuiDebug in appMode then
  begin
    Writeln(StdOut, 'ScreenWidth = ' + IntToStr(ScreenWidth));
    Writeln(StdOut, 'ScreenHeight = ' + IntToStr(ScreenHeight));
    Writeln(StdOut, 'BlocksArea = ' + IntToStr(BlocksArea));
    Writeln(StdOut, 'TotalKiloBytes = ' + IntToStr(TotalKiloBytes) + ' (' + FloatToStr(KiloBytesToMegaBytes(TotalKiloBytes)) + ' MB)');
    Writeln(StdOut, 'KiloBytesInABlock = '  + IntToStr(KiloBytesInABlock) ); //+ FloatToStr(TotalKiloBytes / BlocksArea));
    Writeln(StdOut, 'TotalSystemMemKiloBytes = '  + IntToStr(TotalSystemMemKiloBytes));
    Writeln(StdOut, 'ProcessesCount = '  + IntToStr(ProcessesCount));
    Writeln(StdOut, 'Press <enter> to draw tree map...');
    ReadLn;
  end;

  SortRecords(Processes, ProcessesCount - 1); // biggest last in array

  PrevHeight := 0;
  AtRowWidth := 0;
  AtColHeight := 0;
  color := 1;
  atRow := 0;
  atCol := 0;
  atCount:= 1;
  for at := ProcessesCount - 1 downto 0 do
  //for at := 0 to ProcessesCount - 1 do
  begin
    //Processes[at].Area:= abs(Processes[at].Area); //sqrt hack for abs (for win10 testing)
    Width:= round(sqrt(Processes[at].Area )); // thanks C, mad cred!
    Height:= round(sqrt(Processes[at].Area) / 2);
    Processes[at].Width := Width;
    Processes[at].Height := Width;
    Processes[at].row := atRow;
    Processes[at].col := atCol;
    x1 := 1 + AtRowWidth;    // 416
    y1 := 1 + AtColHeight;
    x2 := 1 + AtRowWidth + Width;
    y2 := 1 + AtColHeight + Height;
    if (x2 >= SavedScreenWidth) or (x1 >= SavedScreenWidth) then
    begin
      // update col vars
      atCol := atCol + 1;
      AtColHeight := 1 + PrevHeight + AtColHeight;
      // update row vars
      AtRowWidth := Width;
      atRow := 0;
      // reset to beginning but next col
      x1:= 1;
      y1:= 1 + AtColHeight;
      x2:= 1 + Width;
      y2:= 1 + AtColHeight + Height;
      // reset
      PrevHeight := 0;
    end
    else
    begin
      // check we want to do if we do not reset to a new column
      AtRowWidth := Width + AtRowWidth + 1;
      if PrevHeight < Height then
          PrevHeight := Height;
      atRow := atRow + 1;
    end;

    TextBackground(color);
    TextColor(White);
    Processes[at].Nr := AtCount;
    Text := IntToStr(AtCount) + '. ' + Processes[at].PMem + '% ' + IntToStr(Round(Processes[at].PMegaBytes)) + 'MB ' + Processes[at].Pid + ' ' + Processes[at].Cmd;
    DrawBlockText(x1,y1,x2,y2, Text);

    color := 1 + color;
    if color = black then
      color := color + 1;
    if color > 7 then
      color := 1;

    atCount := atCount + 1;
  end;
  ResetScreen;
  TextBackground(Black);
  TextColor(White);
  RestCursor;
end;

procedure TCLIApplication.GoogleChartTreeMap;
var
  htmlFilename, Text: AnsiString;
  SL, SLOut: TStringList;
  At: Integer;
begin
  htmlFilename := GetOptionValue('o', 'html');
  if htmlFilename = '' then
    htmlFilename := 'treemap.html';
  SL := StringListFromRes('google-charts-treemap.html');
  SLOut := TStringList.Create;
  try
    //for at := ProcessesCount - 1 downto 0 do
    for at := 0 to ProcessesCount - 1 do
    begin
      // $output .= "['$pid','$name',$size],\n";
      AtCount := at + 1;
      Text := IntToStr(AtCount) + '. ' + Processes[at].PMem + '% ' + IntToStr(Round(Processes[at].PMegaBytes)) + 'MB ' + Processes[at].Pid + ' ' + Processes[at].Cmd;
      SLOut.Add('["' + text + '","' + 'root' + '",' + IntToStr(Round(Processes[at].PMegaBytes)) + '],');
    end;
    SL.text := ReplaceStr( SL.Text, '{{output}}', SLOut.Text);
    SL.SaveToFile(htmlFilename);
  finally
    SL.Free;
    SLOut.Free;
  end;
end;

procedure TCLIApplication.ZabbixDiscovery;
var
  outfile, host, key: string;
  SLOut: TStringList;
  At: Integer;
begin
  SLOut := TStringList.Create;
  try
    //for at := ProcessesCount - 1 downto 0 do
    for at := 0 to ProcessesCount - 1 do
    begin
      SLOut.Add('{"{#NAME}":"' + Processes[at].Cmd + '","{#PID}":"' + Processes[at].pid + '"}');
    end;
    SLOut.QuoteChar := ' ';
    SLOut.Text := '{"data":[' + ReplaceStr(SLOut.DelimitedText, ' ', '')  + ']}';
    outfile := GetOptionValue('o', 'output');
    host := GetOptionValue('h', 'host');
    key := GetOptionValue('k', 'key');
    if (host <> '') and (key = '') then
      raise Exception.Create('Supply --key for --zabbix-discovery with --host option');
    if (host <> '') and (key <> '') then
      SLOut.Text := host + ' ' + key + ' ' + SLOut.Text;
    if outfile = '' then
      writeln(StdOut, SLOut.Text)
    else
      SLOut.SaveToFile(outfile);
  finally
    SLOut.Free;
  end;
end;

procedure TCLIApplication.ZabbixTrapper;
var
  outfile, host: string;
  SLOut: TStringList;
  At: Integer;
begin
  SLOut := TStringList.Create;
  try
    host := GetOptionValue('h', 'host');
    if (host = '') then
      raise Exception.Create('Supply --host id for --zabbix-trapper.');
    for at := 0 to ProcessesCount - 1 do
    begin
      SLOut.Add(host + ' process.memory.usage[' + ReplaceStr(Processes[at].Cmd, ' ' , '') + '][' + Processes[at].pid + '] ' + IntToStr(Processes[at].rss));
    end;
    outfile := GetOptionValue('o', 'output');
    if outfile = '' then
      writeln(StdOut, SLOut.Text)
    else
      SLOut.SaveToFile(outfile);
  finally
    SLOut.Free;
  end;
end;

{$IFDEF WINDOWS} // Windows win64, win32
procedure TCLIApplication.GetProcessLinesFromOSWindows;
begin
  ProcessLines := OsProcesses();
  TotalSystemMemKiloBytes := GetSystemMemKb;
end;
{$ENDIF}

{$IFDEF UNIX}
procedure TCLIApplication.GetProcessLinesFromOSUnix;
var
  psCmd, psArg0, psArg1, psOut: AnsiString;
begin
  psCmd := '/bin/ps';
  psArg0 := '-eo';
  {$IFDEF DARWIN} // macOS
  psArg1 := 'pmem,rss,pid,command';
  {$ENDIF}
  {$IFDEF LINUX}
  psArg1 := 'pmem,rss,pid,cmd';
  {$ENDIF}
  if not RunCommand(psCmd,[psArg0,psArg1], psOut) then
  begin
    psArg1 := 'vsz,rss,pid,comm'; // try to support busy box
    if not RunCommand(psCmd,[psArg0,psArg1], psOut) then
    begin
      writeln('Could not read "' + psFilename + '" or execute "' + psCmd + '" with args ("' + psArg0 + '","' + psArg1 + '").');
      Halt;
    end;
  end;
  ProcessLines := TStringList.Create;
  ProcessLines.Text := psOut;
end;
{$ENDIF}

procedure TCLIApplication.DoRun;
var
  n, rl: string;
begin
  inherited DoRun;
  ProcessLines := nil;
  {$IFDEF WINDOWS}
  appMode := appMode + [tuiWindows];
  SetConsoleOutputCP(CP_UTF8);
  {$ENDIF}
  CheckOptions;
  Main;
  if (tuiReadline in appMode) or
     ( (tuiWindows in appMode) and not ( tuiZabbix in appMode) )then
  begin
    rl:= GetOptionValue('readln');
    if (rl <> '0') or (lowerCase(rl) <> 'off') or (lowerCase(rl) <> 'false') then
       Readln;
  end;
  if Self.HasOption('n', 'interval') then
  begin
    n:= GetOptionValue('n', 'interval');
    while true do
    begin
      SleepCheckCtrlC(StrToInt(n) * 1000);
      CheckCtrlC;
      BlankScreen;
      Main;
      CheckCtrlC;
    end;
  end;
  Terminate;
end;

var
  app: TCLIApplication;

begin
  app:= TCLIApplication.Create(nil);
  app.Title:='fptuitreemap';
  app.Run;
  app.Free;
end.

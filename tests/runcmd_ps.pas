program runcmd_ps;
 
{$mode objfpc}{$H+}
 
uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Classes, SysUtils, CustApp,
  process, crt
  { you can add units after this };
 
type
 
  { TMyApplication }
 
  TMyApplication = class(TCustomApplication)
  protected
    procedure DoRun; override;
  public
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
    procedure WriteHelp; virtual;
  end;
 
{ TMyApplication }
 
procedure TMyApplication.DoRun;
var
  ErrorMsg: String;
  r: boolean;
  sCmd, sArg0, sArg1, sPsOut: AnsiString;
begin
  // quick check parameters
  ErrorMsg:=CheckOptions('h', 'help');
  if ErrorMsg<>'' then begin
    ShowException(Exception.Create(ErrorMsg));
    Terminate;
    Exit;
  end;
 
  // parse parameters
  if HasOption('h', 'help') then begin
    WriteHelp;
    Terminate;
    Exit;
  end;

  sCmd := '/bin/ps';
  sArg0 := '-eo';
  sArg1 := 'pmem,rss,pid,command';
  r := RunCommand(sCmd,[sArg0,sArg1], sPsOut);
  writeln(r);
  // writeln(sPsOut); // mmm maybe not smart to show that to the world in our test
 
  // stop program loop
  Terminate;
end;
 
constructor TMyApplication.Create(TheOwner: TComponent);
begin
  inherited Create(TheOwner);
  StopOnException:=True;
end;
 
destructor TMyApplication.Destroy;
begin
  inherited Destroy;
end;
 
procedure TMyApplication.WriteHelp;
begin
  { add your help code here }
  writeln('Usage: ', ExeName, ' -h');
end;
 
var
  Application: TMyApplication;
begin
  Application:=TMyApplication.Create(nil);
  Application.Title:='My Application';
  Application.Run;
  Application.Free;
end.
